# TP4 : TCP, UDP et services réseau

Dans ce TP on va explorer un peu les protocoles TCP et UDP. On va aussi mettre en place des services qui font appel à ces protocoles.

# 0. Prérequis

➜ Pour ce TP, on va se servir de VMs Rocky Linux. 1Go RAM c'est large large. Vous pouvez redescendre la mémoire vidéo aussi.  

➜ Les firewalls de vos VMs doivent **toujours** être actifs (et donc correctement configurés).

➜ **Si vous voyez le p'tit pote 🦈 c'est qu'il y a un PCAP à produire et à mettre dans votre dépôt git de rendu.**

***TOUT MES FICHIERS PCAPNG SONT DANS LE DOSSIER TP4, JE PRECISE LES NOMS CORRESPONDANTS A CHAQUE FOIS.***

# I. First steps

Faites-vous un petit top 5 des applications que vous utilisez sur votre PC souvent, des applications qui utilisent le réseau : un site que vous visitez souvent, un jeu en ligne, Spotify, j'sais po moi, n'importe.
🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**

*J'utilise ```PS C:\WINDOWS\system32> netstat -n -b ``` afin d'afficher les statistiques de protocole et des connections réseaux actives*

*Pour **Discord** on peut voir que le port et l'adresse IP à laquelle on est connécté sont le port **443**  l'IP est **179.60.192.3** . Le port local lui est **1024**. Il s'agit d'un protocole **TCP**.*
Le fichier wireshark correspondant est **tp4_discord.pcapng**.

*Pour **Opéra** on peut voir que le port et l'adresse IP à laquelle on est connécté sont le port **443**  l'IP est **54.239.28.85** . Le port local lui est **13534**. Il s'agit d'un protocole **TCP**.*
Le fichier wireshark correspondant est **tp4_opera.pcapng**.

*Pour **EpicGameLauncher** on peut voir que le port et l'adresse IP à laquelle on est connécté sont le port **443**  l'IP est **52.222.174.111** . Le port local lui est **5303**. Il s'agit d'un protocole **TCP**.*
Le fichier wireshark correspondant est **tp4_epic.pcapng**.

*Pour **MicrosoftEdge** on peut voir que le port et l'adresse IP à laquelle on est connécté sont le port **443**  l'IP est **204.79.197.239** . Le port local lui est **5367**. Il s'agit d'un protocole **TCP**.*
Le fichier wireshark correspondant est **tp4_microsoft.pcapng**.

*Pour **UPlay** on peut voir que le port et l'adresse IP à laquelle on est connécté sont le port **443**  l'IP est **23.212.224.215** . Le port local lui est **18578**. Il s'agit d'un protocole **TCP**.*
Le fichier wireshark correspondant est **tp4_uplay.pcapng**.


- avec Wireshark, on va faire les chirurgiens réseau
- déterminez, pour chaque application :
  - IP et port du serveur auquel vous vous connectez
  - le port local que vous ouvrez pour vous connecter

> Dès qu'on se connecte à un serveur, notre PC ouvre un port random. Une fois la connexion TCP ou UDP établie, entre le port de notre PC et le port du serveur qui est en écoute, on parle de tunnel TCP ou de tunnel UDP.


> Aussi, TCP ou UDP ? Comment le client sait ? Il sait parce que le serveur a décidé ce qui était le mieux pour tel ou tel type de trafic (un jeu, une page web, etc.) et que le logiciel client est codé pour utiliser TCP ou UDP en conséquence.

🌞 **Demandez l'avis à votre OS**

**LA COMMANDE UTILISEE**
```bash

PS C:\WINDOWS\system32> netstat -b
```

**POUR OPERA**
```bash
TCP    10.188.82.225:1028     wb-in-f188:5228        ESTABLISHED
 [opera.exe]
 ```
 
**POUR DISCORD**
```bash
TCP    10.188.82.225:1025     162.159.136.234:https  ESTABLISHED
 [Discord.exe]
```

**POUR MICROSOFT EDGE**
```bash
TCP    10.188.82.225:9997     server-13-249-9-35:https  ESTABLISHED
 [msedge.exe]
```

**POUR EPIC GAME LAUNCHER**
```bash
TCP    10.188.82.225:10038    ec2-52-1-78-161:https  ESTABLISHED
 [EpicGamesLauncher.exe]
```

**POUR UPLAY**
```bash
TCP    10.188.82.225:1024     server-143-204-231-93:https  ESTABLISHED
 [UplayWebCore.exe]
 ```


🦈🦈🦈🦈🦈 **Bah ouais, captures Wireshark à l'appui évidemment.** Une capture pour chaque application, qui met bien en évidence le trafic en question.

# II. Mise en place

Allumez une VM Linux pour la suite.

## 1. SSH

Connectez-vous en SSH à votre VM.

🌞 **Examinez le trafic dans Wireshark**

- donnez un sens aux infos devant vos yeux, capturez un peu de trafic, et coupez la capture, sélectionnez une trame random et regardez dedans, vous laissez pas brainfuck par Wireshark n_n
*sshPArti2.pcapng*
*ça c'est tout ce qui s'est mis quand j'ai mis un simpe a dans le terminal*
*je suposse que ça va crypter le paquet puis l'envoyer et le decrypter*
- **déterminez si SSH utilise TCP ou UDP**
  - pareil réfléchissez-y deux minutes, logique qu'on utilise pas UDP non ?
  *Mais ça utlise TCP, ou alors j'ai un probleme*
- **repérez le *3-Way Handshake* à l'établissement de la connexion**
  - c'est le `SYN` `SYNACK` `ACK`
  *SYNACKACK.pcapng*
- **repérez le FIN FINACK à la fin d'une connexion**
- entre le *3-way handshake* et l'échange `FIN`, c'est juste une bouillie de caca chiffré, dans un tunnel TCP
*RSTACK.pcapng*

🌞 **Demandez aux OS**

- repérez, avec un commande adaptée, la connexion SSH depuis votre machine
- ET repérez la connexion SSH depuis votre VM

*Depuis la machine*
```bash
bravo@Asus-Valentin MINGW64 ~
$ netstat -n -b -p tcp | grep -i ssh -B 1
  TCP    10.3.1.2:1182          10.3.1.11:22           ESTABLISHED
 [ssh.exe]

```
*Depuis la vm*
```bash
[val@localhost ~]$ ss -l | grep ssh
tcp   LISTEN 0      128                                       0.0.0.0:ssh                     0.0.0.0:*
tcp   LISTEN 0      128                                          [::]:ssh                        [::]:*
```

🦈 **Je veux une capture clean avec le 3-way handshake, un peu de trafic au milieu et une fin de connexion**

## 2. NFS

Allumez une deuxième VM Linux pour cette partie.

Vous allez installer un serveur NFS. Un serveur NFS c'est juste un programme qui écoute sur un port (comme toujours en fait, oèoèoè) et qui propose aux clients d'accéder à des dossiers à travers le réseau.

Une de vos VMs portera donc le serveur NFS, et l'autre utilisera un dossier à travers le réseau.

🌞 **Mettez en place un petit serveur NFS sur l'une des deux VMs**

- j'vais pas ré-écrire la roue, google it, ou [go ici](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=1)
- partagez un dossier que vous avez créé au préalable dans `/srv`
- vérifiez que vous accédez à ce dossier avec l'autre machine : [le client NFS](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=2)

**le fichier n'apparait pas**

> Si besoin, comme d'hab, je peux aider à la compréhension, n'hésitez pas à m'appeler.

🌞 **Wireshark it !**

- une fois que c'est en place, utilisez `tcpdump` pour capturer du trafic NFS
- déterminez le port utilisé par le serveur

🌞 **Demandez aux OS**

- repérez, avec un commande adaptée, la connexion NFS sur le client et sur le serveur

```
# GNU/Linux
$ ss
```

🦈 **Et vous me remettez une capture de trafic NFS** la plus complète possible. J'ai pas dit que je voulais le plus de trames possible, mais juste, ce qu'il faut pour avoir un max d'infos sur le trafic

## 3. DNS

🌞 Utilisez une commande pour effectuer une requête DNS depuis une des VMs

- capturez le trafic avec un `tcpdump`
- déterminez le port et l'IP du serveur DNS auquel vous vous connectez
